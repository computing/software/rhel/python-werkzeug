PACKAGE=$(shell echo *.spec | sed -e 's/.spec$$//')

.PHONY: sources srpm

sources:
	spectool -g $(PACKAGE).spec

srpm: sources
	rpmbuild -D '_srcrpmdir ./' -D '_sourcedir ./' -bs $(PACKAGE).spec
